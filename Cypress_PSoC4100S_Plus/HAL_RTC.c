//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       HAL_RTC.c
\brief      Real time clock handling on hardware base

***********************************************************************************/
#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS
    
/********************************* includes **********************************/
#include "HAL_RTC.h"
//#include "stdio.h"

/********************************* Defines **********************************/
#define WCO_STARTUP_DELAY_CYCLES    (LFCLK_CYCLES_PER_SECOND)
#define TICK_EACH_1_HZ              (1u)
#define UTC_OFFSET                   1
#define TIMEZONE_OFFSET              (UTC_OFFSET * 3600)
#define SECOND_INTERVALL_MILLI      (1000u)

/************************* local function prototypes *************************/
static void EnableRtcOperation(void);
static teSysTimerReturn LinkRtcWithWdt(void);

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/

/*********************************  functions *********************************/

//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Starts RTC-Component and sets the update period.
\param   none
\return  none
***********************************************************************************/
static void EnableRtcOperation(void)
{
    /* Start RTC component */
    RTC_Start();
    
    /* Set RTC time update period */
    RTC_SetPeriod(1, TICK_EACH_1_HZ);
}

//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Initialiize a system timer for the RTC update functionality 
\param   none
\return  eReturnVal - enumeration of specifc return values
***********************************************************************************/
static teSysTimerReturn LinkRtcWithWdt(void)
{
    teSysTimerReturn eReturnVal = eSysTimer_Invalid;
    
    /* Use system timer 0 for RTC */
    eReturnVal = HAL_WDT_SystemTimerInit(eSysTimer0, SECOND_INTERVALL_MILLI);
    
    /* Check if linkage was successfull */
    if(eReturnVal == eSysTimer_Success)
    {
        HAL_WDT_SystemTimerInterruptInit();    
    
        /* Set up a callback function for this system timer */
        eReturnVal = HAL_WDT_SystemTimerSetCallbackFunction(eSysTimer0, HAL_RealTimeClock_Update);
    }
    
    return eReturnVal;    
}


//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Initialize the RTC-Module and link it with a system timer.
\param   none
\return  teSysTimerReturn - Enumeration of the correct handling
***********************************************************************************/
teSysTimerReturn HAL_RealTimeClock_Init(void)
{
    teSysTimerReturn eReturn = eSysTimer_Invalid;
    
    /* Check if WCO is already running */
    if(CySysClkWcoEnabled() == false)
    {
        /* Start WCO */
        CySysClkWcoStart();
    }
       
    /* Link WDT0 with the RTC */
    eReturn = LinkRtcWithWdt();
    
    /* Start RTC operation */
    EnableRtcOperation();
    
    return eReturn;
}


//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Update current clock by previsouly set interval. Shall be used as callback
         function for the system timers.
\param   none
\return  none
***********************************************************************************/
void HAL_RealTimeClock_Update(void)
{
    RTC_Update();
}

//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Gets the time from the RTC and saves it into the given pointers.
\param   pucHour - Pointer where the current hour shall be saved
\param   pucMin - Pointer where the current minutes shall be saved
\param   pulTicks - Pointer where the current unix-tick shall be saved
\return  none
***********************************************************************************/
void HAL_RealTimeClock_GetClock(u8* pucHour, u8* pucMin, u32* pulTicks)
{
    /* Get time from RTC */
    u32 ulTime = RTC_GetTime();
    
    /* Check for correct pointer */
    if(pucHour)
    {
        *pucHour = (u8)RTC_GetHours(ulTime);
    }
    
    if(pucMin)
    {
        *pucMin = (u8)RTC_GetMinutes(ulTime);
    }
    
    if(pulTicks)
    {
        *pulTicks = (u32)RTC_GetUnixTime();
    }
}


//********************************************************************************
/*!
\author  Kraemer E
\date    06.10.2020
\brief   Sets actual epoch time
\param   ulEpochTick - The epoch/unix time
\return  bDSTActive - Returns boolen if the DST(Day-save-time) is active
***********************************************************************************/
void HAL_RealTimeClock_SetTime(u32 ulEpochTick)
{
    RTC_SetUnixTime((ulEpochTick + TIMEZONE_OFFSET));
}

//********************************************************************************
/*!
\author  Kraemer E
\date    06.10.2020
\brief   Gets the status of the day-save-time
\param   none
\return  RTC_STATUS_DST - Returns boolen if the DST(Day-save-time) is active
***********************************************************************************/
bool HAL_RealTimeClock_GetDaySaveTimeStatus(void)
{
    return RTC_STATUS_DST;
}

#endif //PSOC_4100S_PLUS
