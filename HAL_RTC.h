//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       HAL_RTC.h
\brief      Real time clock header file

***********************************************************************************/
#ifndef _HAL_REALTIMECLOCK_H_
#define _HAL_REALTIMECLOCK_H_

#ifdef __cplusplus
extern "C"
{
#endif

/********************************* includes **********************************/
#include "BaseTypes.h"
#include "HAL_Watchdog.h"

/***************************** defines / macros ******************************/

/************************ externally visible functions ***********************/
teSysTimerReturn HAL_RealTimeClock_Init(void);
void HAL_RealTimeClock_Update(void);
void HAL_RealTimeClock_SetTime(u32 ulEpochTick);
void HAL_RealTimeClock_GetClock(u8* pucHour, u8* pucMin, u32* pulTicks);
bool HAL_RealTimeClock_GetDaySaveTimeStatus(void);


#ifdef __cplusplus
}
#endif

#endif // _HAL_REALTIMECLOCK_H_

/* [] END OF FILE */
