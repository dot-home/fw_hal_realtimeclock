//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       HAL_RTC.c
\brief      Real time clock handling on hardware base

***********************************************************************************/
#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266
    
/********************************* includes **********************************/
#include "HAL_RTC.h"
//#include "stdio.h"

/********************************* Defines **********************************/
#define WCO_STARTUP_DELAY_CYCLES    (LFCLK_CYCLES_PER_SECOND)
#define TICK_EACH_1_HZ              (1u)
#define UTC_OFFSET                   1
#define TIMEZONE_OFFSET              (UTC_OFFSET * 3600)
#define SECOND_INTERVALL_MILLI      (1000u)

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/

/*********************************  functions *********************************/



//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Initialize the RTC-Module and link it with a system timer.
\param   none
\return  teSysTimerReturn - Enumeration of the correct handling
***********************************************************************************/
teSysTimerReturn HAL_RealTimeClock_Init(void)
{
    teSysTimerReturn eReturn = eSysTimer_Success;
    
    return eReturn;
}


//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Update current clock by previsouly set interval. Shall be used as callback
         function for the system timers.
\param   none
\return  none
***********************************************************************************/
void HAL_RealTimeClock_Update(void)
{
    //No RTC available
}

//********************************************************************************
/*!
\author  Kraemer E
\date    04.10.2020
\brief   Gets the time from the RTC and saves it into the given pointers.
\param   pucHour - Pointer where the current hour shall be saved
\param   pucMin - Pointer where the current minutes shall be saved
\param   pulTicks - Pointer where the current unix-tick shall be saved
\return  none
***********************************************************************************/
void HAL_RealTimeClock_GetClock(u8* pucHour, u8* pucMin, u32* pulTicks)
{
    /* Get time from RTC */
    //No RTC available
    
    /* Check for correct pointer */
    if(pucHour)
    {
        *pucHour = 0;
    }
    
    if(pucMin)
    {
        *pucMin = 0;
    }
    
    if(pulTicks)
    {
        *pulTicks = 0;
    }
}


//********************************************************************************
/*!
\author  Kraemer E
\date    06.10.2020
\brief   Sets actual epoch time
\param   ulEpochTick - The epoch/unix time
\return  bDSTActive - Returns boolen if the DST(Day-save-time) is active
***********************************************************************************/
void HAL_RealTimeClock_SetTime(u32 ulEpochTick)
{
    //No RTC available
}

//********************************************************************************
/*!
\author  Kraemer E
\date    06.10.2020
\brief   Gets the status of the day-save-time
\param   none
\return  RTC_STATUS_DST - Returns boolen if the DST(Day-save-time) is active
***********************************************************************************/
bool HAL_RealTimeClock_GetDaySaveTimeStatus(void)
{
    return false;
}

#endif //PSOC_4100S_PLUS
